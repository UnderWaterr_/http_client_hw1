package ru.kpfu.itis.minnibaeva;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        HttpClient httpClient = new HttpClientImpl();
        Map<String, String> paramsGet = new HashMap<>();
        paramsGet.put("foo1", "bar1");
        paramsGet.put("foo2", "bar2");

        Map<String, String> headersGet = new HashMap<>();
        headersGet.put("Content-Type", "application/json");

        System.out.println(httpClient.get("https://postman-echo.com/get", headersGet, paramsGet));

        System.out.println("---------------------------------------------------");


        Map<String, String> paramsPost = new HashMap<>();
        paramsPost.put("name", "Alina");
        paramsPost.put("surname", "Minnibaeva");

        Map<String, String> headersPost = new HashMap<>();
        headersPost.put("Content-Type", "application/json");
        headersPost.put("Accept", "text/json");

        System.out.println(httpClient.post("https://postman-echo.com/post", headersPost, paramsPost));

    }
}
