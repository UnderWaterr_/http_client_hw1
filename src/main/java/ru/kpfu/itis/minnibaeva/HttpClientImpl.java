package ru.kpfu.itis.minnibaeva;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class HttpClientImpl implements HttpClient {

    @Override
    public String get(String url, Map<String, String> headers, Map<String, String> params) {
        try {
            URL finalUrl = new URL(setUrlParam(url, params));
            HttpURLConnection connectionGet = (HttpURLConnection) finalUrl.openConnection();
            connectionGet.setRequestMethod("GET");
            setUrlHeaders(connectionGet, headers);

            System.out.println(connectionGet.getResponseCode());

            StringBuilder requestResult;

            try (BufferedReader reader =
                         new BufferedReader(new InputStreamReader(connectionGet.getInputStream()))) {
                requestResult = new StringBuilder();

                String input;
                while ((input = reader.readLine()) != null) {
                    requestResult.append(input);
                }
            }
            connectionGet.disconnect();

            return requestResult.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public String post(String url, Map<String, String> headers, Map<String, String> params) {
        try {
            URL finalUrl = new URL(url);
            HttpURLConnection connectionPost = (HttpURLConnection) finalUrl.openConnection();

            connectionPost.setRequestMethod("POST");
            setUrlHeaders(connectionPost, headers);

            String jsonInputString = createJsonStringForPostRequest(params);
            System.out.println(jsonInputString);

            connectionPost.setDoOutput(true);
            try (DataOutputStream dataOutputStream = new DataOutputStream(connectionPost.getOutputStream())) {
                byte[] string = jsonInputString.getBytes(StandardCharsets.UTF_8);
                dataOutputStream.write(string);
            }

            System.out.println(connectionPost.getResponseCode());

            StringBuilder requestResult = new StringBuilder();
            try (BufferedReader bufferedReader =
                         new BufferedReader(new InputStreamReader(connectionPost.getInputStream()))) {
                String appendString;
                while ((appendString = bufferedReader.readLine()) != null) {
                    requestResult.append(appendString);
                }
            }
            return requestResult.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String createJsonStringForPostRequest(Map<String, String> params) {

        if (params != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{");
            for (String key : params.keySet()) {
                stringBuilder.append("\"").append(key).append("\":")
                        .append("\"").append(params.get(key)).append("\"").append(", ");
            }
            stringBuilder.delete(stringBuilder.length()-2, stringBuilder.length());
            stringBuilder.append("}");
            return stringBuilder.toString();
        }
        return "";
    }

    private void setUrlHeaders(HttpURLConnection connection, Map<String, String> headers) {
        if (headers != null) {
            for (String key : headers.keySet()) {
                connection.setRequestProperty(key, headers.get(key));
            }
        }
    }


    private String setUrlParam(String url, Map<String, String> param) {
        if (param != null) {
            StringBuilder stringBuilder = new StringBuilder(url + "?");

            for (String key : param.keySet()) {
                stringBuilder.append(key).append("=").append(param.get(key)).append("&");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            return stringBuilder.toString();

        }
        return url;
    }
}